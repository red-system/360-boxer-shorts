<footer class="section padding-bottom-16 is-dark no-padding-top">
        <div class="container margin-bottom-double is-wide">
            <div class="col lg-12 no-margin-bottom">
                <div class="hr on-dark"></div>
            </div>
        </div>
        <div class="container margin-bottom is-wide">
            <div class="col lg-9 md-12 no-margin-bottom-lg"><img src="asset/images/Logo-Putih.png" alt="" class="margin-bottom ft-img">
                <!-- <div class="low-text-contrast margin-bottom">Jl. &#x27;Alma No.2 <br>Semanding 35, Surabaia<br>Tel: +6281 234 567<br>hello@zuma.com</div>
                <div class="flex-horizontal low-text-contrast">
                    <div class="fa-brand margin-right-small"><a href="#"></a></div>
                    <div class="fa-brand margin-right-small"><a href="#"></a></div>
                    <div class="fa-brand margin-right-small"><a href="#"></a></div>
                    <div class="fa-brand margin-right-small"><a href="#"></a></div>
                </div> -->
            </div>
            <!-- <div class="col lg-8 md-12">
                <div class="container container-nested">
                    <div class="col lg-4 md-12 no-margin-bottom-lg">
                        <h4 class="on-dark">Categories</h4><a href="#" class="footer-nav-link on-dark">Accent chair</a><a href="#" class="footer-nav-link on-dark">Ottoman</a><a href="#" class="footer-nav-link on-dark">Folding</a><a href="#" class="footer-nav-link on-dark">Blluga suede</a>
                    </div>
                    <div class="col lg-4 md-12 no-margin-bottom-lg">
                        <h4 class="on-dark">Important links</h4><a href="#" class="footer-nav-link on-dark">Shipping &amp; return</a><a href="#" class="footer-nav-link on-dark">Affiliate</a><a href="#" class="footer-nav-link on-dark">GDPR</a><a href="#" class="footer-nav-link on-dark">Tax and handling</a>
                    </div>
                    <div class="col lg-4 md-12 no-margin-bottom">
                        <h4 class="on-dark">Company</h4><a href="#" class="footer-nav-link on-dark">About</a><a href="#" class="footer-nav-link on-dark">Contact</a><a href="#" class="footer-nav-link on-dark">Location</a><a href="#" class="footer-nav-link on-dark">Carreer</a>
                    </div>
                </div>
            </div> -->
            <div class="col lg-3 md-12 flex-vertical no-margin-bottom">
                        <div class="flexh-space-between low-text-contrast">
                            <!-- <div class="fa-brand margin-right-small"><a href="#" style="color:#fff;"></a></div> -->
                            <!-- <div class="fa-brand margin-right-small"><a href="#" style="color:#fff;"></a></div> -->
                            <div class="fa-brand margin-right-small"><a href="https://www.facebook.com/360boxershorts" style="color:#ddd; font-size:12pt;">  @360boxershorts</a></div>
                            <!-- <div class="fa-brand margin-right-small"><a href="#"style="color:#fff;"></a></div> -->
                            <!-- <div class="fa-brand margin-right-small"><a href="#"style="color:#fff;"></a></div> -->
                            &nbsp&nbsp<div class="fa-brand margin-right-small"><a href="https://www.instagram.com/360boxershorts/"style="color:#ddd; font-size:12pt;">  360boxershorts</a></div>
                        </div>
                <!-- <h4 class="margin-bottom on-dark">Get Our Store App </h4><img src="https://via.placeholder.com/1000x600.png?text=IMAGE" alt="" class="max-width-200px"> -->
            </div>
        </div>
        <!-- <div class="container is-wide">
            <div class="col lg-12 margin-bottom">
                <div class="hr on-dark"></div>
            </div>
        </div> -->
    </footer>
    <!-- <div class="c-overlay">
        <div class="c-overlay__modal">
            <div class="container">
                <div class="col no-margin-bottom lg-5 sm-12 sm-margin-bottom">
                    <div data-animation="slide" data-duration="500" data-infinite="1" class="c-slider w-slider">
                        <div class="w-slider-mask">
                            <div class="w-slide"><img src="https://via.placeholder.com/1000x600.png?text=IMAGE 560w" sizes="100vw" alt=""></div>
                            <div class="w-slide"><img src="https://via.placeholder.com/1000x600.png?text=IMAGE 560w" sizes="100vw" alt=""></div>
                            <div class="w-slide"><img src="https://via.placeholder.com/1000x600.png?text=IMAGE 560w" sizes="100vw" alt=""></div>
                        </div>
                        <div class="c-slider__left-arrow w-slider-arrow-left">
                            <div class="w-icon-slider-left"></div>
                        </div>
                        <div class="c-slider__right-arrow w-slider-arrow-right">
                            <div class="w-icon-slider-right"></div>
                        </div>
                        <div class="c-slider__nav w-slider-nav w-slider-nav-invert w-round"></div>
                    </div>
                </div>
                <div class="col lg-1"></div>
                <div class="col no-margin-bottom lg-6 sm-12">
                    <div class="size-h3 margin-bottom-small">Merchant checked shirt in black</div>
                    <div class="c-product__rating margin-bottom">
                        <div class="fa is-filled"></div>
                        <div class="fa is-filled"></div>
                        <div class="fa is-filled"></div>
                        <div class="fa is-filled"></div>
                        <div class="fa is-blank"></div>
                    </div>
                    <div class="size-h2 margin-bottom">$120</div>
                    <p>Vestibulum id ligula porta felis euismod semper. Morbi leo risus, porta ac consectetur acidisping vestibulum at eros. Duis mollis, est non sepulaids</p>
                    <div class="w-form">
                        <form id="wf-form-Add-to-cart" name="wf-form-Add-to-cart" data-name="Add to cart" method="post">
                            <div class="flexh-align-center">
                                <div class="flexh-align-center margin-right"><a href="#" class="quantity-button is-minus">–</a><input type="text" maxlength="256" name="field" data-name="Field" required="" class="quantity-input w-input"><a href="#" class="quantity-button is-plus">+</a></div>
                                <a href="#" class="button-primary is-small w-inline-block">
                                    <div class="button-primary-text">add to cart</div>
                                </a>
                            </div>
                        </form>
                        <div class="w-form-done">
                            <div>Thank you! Your submission has been received!</div>
                        </div>
                        <div class="w-form-fail">
                            <div>Oops! Something went wrong while submitting the form.</div>
                        </div>
                    </div>
                </div>
            </div>
            <a data-w-id="202df1b2-468b-88dd-04a0-44fffda14bc3" href="#" class="c-close-btn w-inline-block">
                <div class="iconfont"><em class="iconfont__no-italize"></em></div>
            </a>
        </div>
    </div> -->
    <script src="https://d3e54v103j8qbb.cloudfront.net/js/jquery-3.4.1.min.220afd743d.js" type="text/javascript" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="asset/js/zuma.js" type="text/javascript"></script>
    <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
</body>

</html>