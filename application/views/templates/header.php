<!DOCTYPE html>
<!--  Last Published: Thu Nov 07 2019 03:09:48 GMT+0000 (UTC)  -->
<html data-wf-page="5db7ae94fce786ee9399af97" data-wf-site="5db7ae94fce7860a5799af95">

<head>
    <meta charset="utf-8">
    <title>Zuma — Premium eCommerce template for your online shop</title>
    <meta content="Zuma —  Premium eCommerce template for your online shop" property="og:title">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <link href="asset/css/normalize.css" rel="stylesheet" type="text/css">
    <link href="asset/css/components.css" rel="stylesheet" type="text/css">
    <link href="asset/css/zuma.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css2?family=Dancing+Script:wght@700&display=swap" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js" type="text/javascript"></script>
    <script type="text/javascript">
    WebFont.load({ google: { families: ["Montserrat:100,100italic,200,200italic,300,300italic,400,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic", "Gothic A1:100,200,300,regular,500,600,700,800,900"] } });
    </script>
    <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
    <script type="text/javascript">
    ! function(o, c) { var n = c.documentElement,
            t = " w-mod-";
        n.className += t + "js", ("ontouchstart" in o || o.DocumentTouch && c instanceof DocumentTouch) && (n.className += t + "touch") }(window, document);
    </script>
    <link href="https://via.placeholder.com/1000x600.png?text=IMAGE" rel="shortcut icon" type="image/x-icon">
    <link href="https://via.placeholder.com/1000x600.png?text=IMAGE" rel="apple-touch-icon">
</head>

<body>
    <header class="navigation-section">
        <div class="navigation-overlay"></div>
        <div class="navigation-and-offcanvas">
            <div class="col lg-2 md-basis-auto md-order-first no-margin-bottom-lg"><a href="index.html" class="brand justify-left w-inline-block w--current"><img src="asset/images/-asset-zuma-dark.svg" alt=""></a></div>
            <div class="col no-margin-bottom lg-8 md-basis-auto md-order-last">
                <nav class="navigation-menu flexh-justify-center position-relative md-margin-top"><a href="index.html" class="nav-link on-dark w--current">Home</a>
                    <div data-hover="1" data-delay="0" data-w-id="19441b04-6667-8112-1ac3-50ce8ef78c0d" class="nav-dropdown has-megamenu w-dropdown">
                        <div class="nav-dropdown-toggle w-dropdown-toggle">
                            <a href="financial/services.html" class="nav-link-block on-dark w-inline-block">
                                <div>Shop</div>
                                <div class="dropdown-icon w-icon-dropdown-toggle"></div>
                            </a>
                        </div>
                        <nav class="c-megadropdown w-dropdown-list">
                            <div class="container container-nested">
                                <div class="col lg-3 md-12 no-margin-bottom-lg">
                                    <h4 class="dropdown-title">Categories</h4><a href="#" class="c-megadropdown_link w-dropdown-link">Clothing</a><a href="#" class="c-megadropdown_link w-dropdown-link">Shoes</a><a href="#" class="c-megadropdown_link w-dropdown-link">Cologne</a><a href="#" class="c-megadropdown_link w-dropdown-link">Accessories</a>
                                </div>
                                <div class="col lg-3 md-12 no-margin-bottom-lg">
                                    <h4 class="dropdown-title">Product layout </h4><a href="single-product.html" class="c-megadropdown_link w-dropdown-link">Standard</a><a href="single-product-with-scrolling-photos.html" class="c-megadropdown_link w-dropdown-link">Big photos</a><a href="single-product-w-left-sidebar.html" class="c-megadropdown_link w-dropdown-link">Left sidebar</a><a href="single-product-w-right-sidebar.html" class="c-megadropdown_link w-dropdown-link">Right sidebar</a>
                                </div>
                                <div class="col lg-3 md-12 no-margin-bottom-lg">
                                    <h4 class="dropdown-title">Shop grid style</h4><a href="shop-2-col.html" class="c-megadropdown_link w-dropdown-link">2 columns</a><a href="shop-3-col.html" class="c-megadropdown_link w-dropdown-link">3 columns</a><a href="shop-4-col.html" class="c-megadropdown_link w-dropdown-link">4 columns</a><a href="shop-5-col-fullwidth.html" class="c-megadropdown_link w-dropdown-link">5 cols &amp; fullwidth</a>
                                </div>
                                <div class="col lg-3 md-12 no-margin-bottom-lg">
                                    <h4 class="dropdown-title">Pages</h4><a href="cart.html" class="c-megadropdown_link w-dropdown-link">Cart</a><a href="checkout.html" class="c-megadropdown_link w-dropdown-link">Checkout</a><a href="order-complete.html" class="c-megadropdown_link w-dropdown-link">Order complete</a><a href="login-registration.html" class="c-megadropdown_link w-dropdown-link">Login / Register</a>
                                </div>
                            </div>
                        </nav>
                    </div><a href="about.html" class="nav-link on-dark">About</a><a href="blog.html" class="nav-link on-dark">Blog</a><a href="contact.html" class="nav-link on-dark">Contact</a>
                    <div data-hover="1" data-delay="0" data-w-id="19441b04-6667-8112-1ac3-50ce8ef78c47" class="nav-dropdown w-dropdown">
                        <div class="nav-dropdown-toggle w-dropdown-toggle">
                            <a href="financial/services.html" class="nav-link-block on-dark w-inline-block">
                                <div>Pages</div>
                                <div class="dropdown-icon w-icon-dropdown-toggle"></div>
                            </a>
                        </div>
                        <nav class="dropdown-list w-dropdown-list"><a href="lookbook.html" class="dropdown-link w-dropdown-link">Lookbook</a><a href="single-post.html" class="dropdown-link w-dropdown-link">Single post</a></nav>
                    </div>
                </nav>
            </div>
            <div class="col no-margin-bottom lg-2 lg-text-align-right md-basis-auto">
                <div class="inline-block position-relative"><a data-w-id="19441b04-6667-8112-1ac3-50ce8ef78c54" href="#" class="c-navbar-icon is-brand-color on-dark"></a>
                    <div class="c-search-popup w-form">
                        <form id="wf-form-Search" name="wf-form-Search" data-name="Search"><input type="text" class="form-input-text w-input" maxlength="256" name="Search-2" data-name="Search 2" placeholder="Search product" id="Search-2"></form>
                        <div class="w-form-done">
                            <div>Thank you! Your submission has been received!</div>
                        </div>
                        <div class="w-form-fail">
                            <div>Oops! Something went wrong while submitting the form.</div>
                        </div>
                        <a data-w-id="19441b04-6667-8112-1ac3-50ce8ef78c5f" href="#" class="c-search-popup__close-btn w-inline-block">
                            <div><em class="iconfont__no-italize"></em></div>
                        </a>
                    </div>
                </div><a href="login-registration.html" class="c-navbar-icon is-19px on-dark"></a>
                <div data-w-id="19441b04-6667-8112-1ac3-50ce8ef78c65" class="c-cart"><a href="#" class="c-navbar-icon on-dark"></a>
                    <div class="c-cart__dropdown">
                        <div class="c-cart__item">
                            <a href="#" class="c-cart__remove-btn w-inline-block">
                                <div class="iconfont"><em class="iconfont__no-italize"></em></div>
                            </a><img src="https://via.placeholder.com/1000x600.png?text=IMAGE" width="64" alt="" class="c-cart__thumbnail">
                            <div class="text-align-left text-small flexv-space-between">
                                <div class="is-heading-color margin-bottom-xsmall md-text-xsmall">Sorrento Retro Wooden Armchair</div>
                                <div class="weight-is-medium low-text-contrast">1 × $120</div>
                            </div>
                        </div>
                        <div class="c-cart__item no-border-bottom">
                            <a href="#" class="c-cart__remove-btn w-inline-block">
                                <div class="iconfont"><em class="iconfont__no-italize"></em></div>
                            </a><img src="https://via.placeholder.com/1000x600.png?text=IMAGE" width="64" alt="" class="c-cart__thumbnail">
                            <div class="text-align-left text-small flexv-space-between">
                                <div class="is-heading-color margin-bottom-xsmall md-text-xsmall">Loris Contemporary Linen Wingback </div>
                                <div class="weight-is-medium low-text-contrast">1 × $120</div>
                            </div>
                        </div>
                        <div class="c-cart__section">
                            <div>Subtotal : </div>
                            <div>$240</div>
                        </div>
                        <div class="c-cart__buttons">
                            <a href="#" class="button-primary is-small min-width-100px is-ghost md-margin-bottom-small w-inline-block">
                                <div class="button-primary-text flexv-justify-center">view Cart</div>
                            </a>
                            <a href="#" class="button-primary is-small min-width-100px w-inline-block">
                                <div class="button-primary-text flexv-justify-center">Checkout</div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <a data-w-id="19441b04-6667-8112-1ac3-50ce8ef78c8b" href="#" class="c-nav__close-button w-inline-block">
                <div class="iconfont is-offcanvas-close-button"><em class="iconfont__no-italize"></em></div>
            </a>
        </div>
        <div class="mobile-navigation-bar"><a href="#" class="brand w-inline-block"><img src="images/-asset-zuma-light.svg" alt="" class="logo-image"></a>
            <a data-w-id="19441b04-6667-8112-1ac3-50ce8ef78c92" href="#" class="burger-button w-inline-block">
                <div class="iconfont is-burger"><em class="iconfont__no-italize"></em></div>
            </a>
        </div>
    </header>
    <div class="section position-relative no-padding-top-bottom">
        <div class="container is-fullwidth">
            <div class="col lg-12 lg-no-padding-lr no-margin-bottom">
                <div data-delay="3000" data-animation="fade" data-autoplay="1" data-easing="ease-out-expo" data-duration="900" data-infinite="1" class="c-heroslider w-slider">
                    <div class="c-heroslider_mask w-slider-mask">
                        <div class="c-heroslider__slide w-slide">
                            <div class="c-heroslider__content bg-color-1">
                                <div class="container is-wide">
                                    <div class="col lg-7 md-12 position-relative">
                                        <h2 class="heading-alpha on-dark">Big Dazzle After Dark</h2>
                                        <div class="flexh-align-center xs-is-wrapping overflow-hidden">
                                            <div class="c-heroslider__discount">27%<br>OFF</div>
                                            <a data-w-id="382fd6e7-7654-e195-4458-f597a99853b8" href="#" class="button-primary animated is-white margin-left w-inline-block">
                                                <div style="-webkit-transform:translate3d(0, 0PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-moz-transform:translate3d(0, 0PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-ms-transform:translate3d(0, 0PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);transform:translate3d(0, 0PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);opacity:1" class="button-primary-text">shop collection</div>
                                                <div style="opacity:0;display:block;-webkit-transform:translate3d(0, 20PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-moz-transform:translate3d(0, 20PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-ms-transform:translate3d(0, 20PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);transform:translate3d(0, 20PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0)" class="button-primary-text for-hover">let&#x27;s go <span class="fa margin-left"></span></div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col lg-5"></div>
                                </div>
                                <img src="http://detheme.com/templates/zuma/images/burshbg30.8x.png" width="1000" sizes="(max-width: 991px) 100vw, 1000px" alt="" class="hero-slide-image alignself-center brush-bg-1 hidden-md">
                                <img src="http://detheme.com/templates/zuma/images/13600.png" width="500" alt="" class="hero-slide-image alignself-center is-model">
                            </div>
                        </div>
                        <div class="c-heroslider__slide w-slide">
                            <div class="c-heroslider__content bg-color-1">
                                <div class="container is-wide">
                                    <div class="col lg-7 md-12 position-relative">
                                        <h2 class="heading-alpha on-dark">Super savvy <br>limited time flash sale</h2>
                                        <div class="flexh-align-center xs-is-wrapping overflow-hidden">
                                            <div class="c-heroslider__discount">77%<br>OFF</div>
                                            <a data-w-id="45adf567-26fe-3f0e-42cb-a25b822a29f3" href="#" class="button-primary animated is-white margin-left w-inline-block">
                                                <div style="-webkit-transform:translate3d(0, 0PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-moz-transform:translate3d(0, 0PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-ms-transform:translate3d(0, 0PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);transform:translate3d(0, 0PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);opacity:1" class="button-primary-text">shop collection</div>
                                                <div style="opacity:0;display:block;-webkit-transform:translate3d(0, 20PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-moz-transform:translate3d(0, 20PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-ms-transform:translate3d(0, 20PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);transform:translate3d(0, 20PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0)" class="button-primary-text for-hover">let&#x27;s go <span class="fa margin-left"></span></div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col lg-5"></div>
                                </div><img src="http://detheme.com/templates/zuma/images/burshbg30.8x.png" width="460" alt="" class="hero-slide-image alignself-center is-model">
                            </div>
                        </div>
                        <div class="c-heroslider__slide w-slide">
                            <div class="c-heroslider__content bg-color-1">
                                <div class="container is-wide">
                                    <div class="col lg-7 md-12 position-relative">
                                        <h2 class="heading-alpha on-dark">Be a dapper consciously</h2>
                                        <div class="flexh-align-center xs-is-wrapping overflow-hidden">
                                            <div class="c-heroslider__discount">20%<br>OFF</div>
                                            <a data-w-id="ede8f71a-b993-aab6-b6e5-702e1ebddbf6" href="#" class="button-primary animated is-white margin-left w-inline-block">
                                                <div style="-webkit-transform:translate3d(0, 0PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-moz-transform:translate3d(0, 0PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-ms-transform:translate3d(0, 0PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);transform:translate3d(0, 0PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);opacity:1" class="button-primary-text">shop collection</div>
                                                <div style="opacity:0;display:block;-webkit-transform:translate3d(0, 20PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-moz-transform:translate3d(0, 20PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-ms-transform:translate3d(0, 20PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);transform:translate3d(0, 20PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0)" class="button-primary-text for-hover">let&#x27;s go <span class="fa margin-left"></span></div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col lg-5"></div>
                                </div><img src="http://detheme.com/templates/zuma/images/burshbg30.8x.png" sizes="(max-width: 479px) 97vw, (max-width: 767px) 90vw, 500px" alt="" class="hero-slide-image alignself-center is-model">
                            </div>
                        </div>
                    </div>
                    <div class="hidden w-slider-arrow-left">
                        <div class="w-icon-slider-left"></div>
                    </div>
                    <div class="hidden w-slider-arrow-right">
                        <div class="w-icon-slider-right"></div>
                    </div>
                    <div class="c-heroslider__nav w-slider-nav w-round"></div>
                </div>
            </div>
        </div>
    </div>