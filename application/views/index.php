
    <div class="section">
        <div class="container">
            <div class="col block-centered text-align-center lg-6 md-12">
            <img src="asset/images/Logo-Hitam.png" alt="three sixty boxer shorts">
                <h2 class="size-h1"></h2>
            </div>
        </div>
        <div class="container position-relative is-wide">
            <div class="col lg-12">
                <div class="w-layout-grid c-grid2x4">
                    <a id="w-node-7f9b90f86253-9399af97" data-w-id="19ccc1ed-fe9e-99e5-db47-7f9b90f86253" href="#" class="link-block w-inline-block">
                        <div class="c-gridgallery1__content-2">
                            <div class="on-dark text-medium weight-is-medium size-h2">Winter pastels</div>
                            <div class="on-dark margin-bottom-double">Saat sedang bersepeda santai</div>
                            <!-- <div class="flexh-align-center">
                                <div class="on-dark margin-right-small text-small">Browse trend</div>
                                <div class="iconfont on-dark is-11px"><em class="iconfont__no-italize"></em></div>
                            </div> -->
                        </div>
                        <div class="c-gridgallery1__image img3"></div>
                    </a>
                    <a id="w-node-7f9b90f8625e-9399af97" data-w-id="19ccc1ed-fe9e-99e5-db47-7f9b90f8625e" href="#" class="link-block w-inline-block">
                        <div class="c-gridgallery1__content-2">
                            <div class="on-dark text-medium weight-is-medium size-h2">The exclusive</div>
                            <div class="on-dark margin-bottom-double">Tidak mudah robek</div>
                            <!-- <div class="flexh-align-center">
                                <div class="on-dark margin-right-small text-small">View collections</div>
                                <div class="iconfont on-dark is-11px"><em class="iconfont__no-italize"></em></div>
                            </div> -->
                        </div>
                        <div class="c-gridgallery1__image img4"></div>
                    </a>
                    <a id="w-node-7f9b90f86269-9399af97" data-w-id="19ccc1ed-fe9e-99e5-db47-7f9b90f86269" href="#" class="link-block w-inline-block">
                        <div class="c-gridgallery1__content-2">
                            <div class="on-dark text-medium weight-is-medium size-h2">The finishing touch </div>
                            <div class="on-dark margin-bottom-double">Tetap santai menjalani aktivitas</div>
                            <!-- <div class="flexh-align-center">
                                <div class="on-dark margin-right-small text-small">Shop accessories </div>
                                <div class="iconfont on-dark is-11px"><em class="iconfont__no-italize"></em></div>
                            </div> -->
                        </div>
                        <div class="c-gridgallery1__image img6"></div>
                    </a>
                    <a id="w-node-7f9b90f86274-9399af97" data-w-id="19ccc1ed-fe9e-99e5-db47-7f9b90f86274" href="#" class="link-block w-inline-block">
                        <div class="c-gridgallery1__content-2">
                            <div class="on-dark text-medium weight-is-medium size-h2">Sweatshirt</div>
                            <div class="margin-bottom-double on-dark">Mudah untuk disimpan</div>
                            <!-- <div class="flexh-align-center">
                                <div class="on-dark margin-right-small text-small">Shop now </div>
                                <div class="iconfont on-dark is-11px"><em class="iconfont__no-italize"></em></div>
                            </div> -->
                        </div>
                        <div class="c-gridgallery1__image img5"></div>
                    </a>
                </div>
            </div>
        </div>
    </div>
    
    <div class="section position-relative">
        <div class="container is-wide">
            <div class="col block-centered text-align-center lg-6 md-12">
                <h2 class="size-h2">Collections</h2>
                <div class="low-text-contrast" style="color:#000;">Explore our collections</div>
            </div>
        </div>
        <div class="container is-wide">
            <div class="col lg-12">
                <div class="w-layout-grid c-grid1x5">
                    <div class="c-product-thumb">
                        <div class="c-product-thumb__top"><a href="#" data-toggle="modal" data-target="#myModal1" class="margin-bottom-small w-inline-block"><img src="asset/images/Katalog-1.jpg" style="width:500px;" alt=""></a>
                            <!-- <div class="text-xsmall weight-is-medium margin-bottom-small"><a href="#">Shirt</a>, <a href="#">Casual</a></div> -->
                            <!-- <div class="c-product__rating">
                                <div class="fa is-filled"></div> 
                                <div class="fa is-filled"></div>
                                <div class="fa is-filled"></div>
                                <div class="fa is-filled"></div>
                                <div class="fa is-blank"></div>
                            </div> -->
                            <a href="single-product.html" class="w-inline-block">
                                <div class="c-product__title">CELANA BOXER PRIA SHARK KIDDOS</div>
                            </a>
                        </div>
                        <!-- <div class="c-product__price-wrapper">
                            <div class="c-product__price margin-right-small">$42</div>
                            <div class="c-product__price is-slashed">$62</div>
                        </div>
                        <div class="c-product__quickview">
                            <a href="#" class="button-primary is-small w-inline-block">
                                <div class="button-primary-text"><span class="fa-regular is-16px"></span>  Quick view</div>
                            </a>
                        </div> -->
                    </div>
                    <div class="c-product-thumb">
                        <div class="c-product-thumb__top"><a href="#" data-toggle="modal" data-target="#myModal2" class="margin-bottom-small w-inline-block"><img src="asset/images/Katalog-2.jpg" style="width:500px;" alt=""></a>
                            <!-- <div class="text-xsmall weight-is-medium margin-bottom-small"><a href="#">Shirt</a>, <a href="#">Casual</a></div> -->
                            <!-- <div class="c-product__rating">
                                <div class="fa is-filled"></div>
                                <div class="fa is-filled"></div>
                                <div class="fa is-filled"></div>
                                <div class="fa is-filled"></div>
                                <div class="fa is-blank"></div>
                            </div> -->
                            <a href="single-product.html" class="w-inline-block">
                                <div class="c-product__title">CELANA BOXER PRIA FAST FOOD - Ukuran, M</div>
                            </a>
                        </div>
                        <!-- <div class="c-product__price-wrapper">
                            <div class="c-product__price margin-right-small">$32</div>
                        </div>
                        <div class="c-product__quickview">
                            <a href="#" class="button-primary is-small w-inline-block">
                                <div class="button-primary-text"><span class="fa-regular is-16px"></span>  Quick view</div>
                            </a>
                        </div> -->
                    </div>
                    <div class="c-product-thumb">
                        <div class="c-product-thumb__top"><a href="#" data-toggle="modal" data-target="#myModal3" class="margin-bottom-small w-inline-block"><img src="asset/images/Katalog-3.jpg" style="width:500px;" alt=""></a>
                            <!-- <div class="text-xsmall weight-is-medium margin-bottom-small"><a href="#">Shirt</a>, <a href="#">Casual</a></div> -->
                            <!-- <div class="c-product__rating">
                                <div class="fa is-filled"></div>
                                <div class="fa is-filled"></div>
                                <div class="fa is-filled"></div>
                                <div class="fa is-filled"></div>
                                <div class="fa is-blank"></div>
                            </div> -->
                            <a href="single-product.html" class="w-inline-block">
                                <div class="c-product__title">CELANA BOXER PRIA MANLY GREYS - M</div>
                            </a>
                        </div>
                        <!-- <div class="c-product__price-wrapper">
                            <div class="c-product__price margin-right-small">$22</div>
                            <div class="c-product__price is-slashed">$32</div>
                        </div>
                        <div class="c-product__quickview">
                            <a href="#" class="button-primary is-small w-inline-block">
                                <div class="button-primary-text"><span class="fa-regular is-16px"></span>  Quick view</div>
                            </a>
                        </div> -->
                    </div>
                    <div class="c-product-thumb">
                        <div class="c-product-thumb__top"><a href="#" data-toggle="modal" data-target="#myModal4" class="margin-bottom-small w-inline-block"><img src="asset/images/kat-04.jpeg" style="width:500px;" alt=""></a>
                            <!-- <div class="text-xsmall weight-is-medium margin-bottom-small"><a href="#">Shirt</a>, <a href="#">Casual</a></div> -->
                            <!-- <div class="c-product__rating">
                                <div class="fa is-filled"></div>
                                <div class="fa is-filled"></div>
                                <div class="fa is-filled"></div>
                                <div class="fa is-filled"></div>
                                <div class="fa is-blank"></div>
                            </div> -->
                            <a href="single-product.html" class="w-inline-block">
                                <div class="c-product__title">CELANA BOXER PRIA BLUESTERA</div>
                            </a>
                        </div>
                        <!-- <div class="c-product__price-wrapper">
                            <div class="c-product__price margin-right-small">$54</div>
                            <div class="c-product__price is-slashed">$60</div>
                        </div>
                        <div class="c-product__quickview">
                            <a href="#" class="button-primary is-small w-inline-block">
                                <div class="button-primary-text"><span class="fa-regular is-16px"></span>  Quick view</div>
                            </a>
                        </div> -->
                    </div>
                    <div class="c-product-thumb">
                        <div class="c-product-thumb__top"><a href="#" data-toggle="modal" data-target="#myModal5" class="margin-bottom-small w-inline-block"><img src="asset/images/Katalog-5.jpg" style="width:500px;" alt=""></a>
                            <!-- <div class="text-xsmall weight-is-medium margin-bottom-small"><a href="#">Shirt</a>, <a href="#">Casual</a></div> -->
                            <!-- <div class="c-product__rating">
                                <div class="fa is-filled"></div>
                                <div class="fa is-filled"></div>
                                <div class="fa is-filled"></div>
                                <div class="fa is-filled"></div>
                                <div class="fa is-blank"></div>
                            </div> -->
                            <a href="single-product.html" class="w-inline-block">
                                <div class="c-product__title">CELANA BOXER PRIA OLIVE PHAPILOS - M</div>
                            </a>
                        </div>
                        <!-- <div class="c-product__price-wrapper">
                            <div class="c-product__price margin-right-small">$20</div>
                        </div>
                        <div class="c-product__quickview">
                            <a href="#" class="button-primary is-small w-inline-block">
                                <div class="button-primary-text"><span class="fa-regular is-16px"></span>  Quick view</div>
                            </a>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="section no-padding-top-bottom overflow-hidden">
        <div class="container is-wide">
            <div class="col lg-12 no-margin-bottom position-relative no-padding-lr overflow-hidden"><img src="https://via.placeholder.com/1000x600.png?text=IMAGE 2309w" sizes="(max-width: 991px) 100vw, 1400px" alt="" class="position-absolute cover-object">
                <div class="container container-nested c-overlay-content">
                    <div class="col lg-6"></div>
                    <div class="col lg-4 lg-md-sm-no-margin-bottom xs-12">
                        <div class="on-dark size-h1 margin-bottom-small">Experience our new flagship</div>
                        <div class="text-large margin-bottom on-dark">Now open at 77th &amp; Broadway NY</div>
                        <a data-w-id="93d51ea5-d4cf-8611-f2da-228b237c60fb" href="#" class="button-primary animated is-white w-inline-block">
                            <div style="-webkit-transform:translate3d(0, 0PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-moz-transform:translate3d(0, 0PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-ms-transform:translate3d(0, 0PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);transform:translate3d(0, 0PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);opacity:1" class="button-primary-text">find out more</div>
                            <div style="opacity:0;display:block;-webkit-transform:translate3d(0, 20PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-moz-transform:translate3d(0, 20PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-ms-transform:translate3d(0, 20PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);transform:translate3d(0, 20PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0)" class="button-primary-text for-hover">let&#x27;s go <span class="fa margin-left"></span></div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- <div class="section">
        <div class="container">
            <div class="col lg-12">
                <div class="w-layout-grid c-grid4x2">
                    <div class="fa-brand _70px"></div>
                    <div class="fa-brand _70px"></div>
                    <div class="fa-brand _70px"></div>
                    <div class="fa-brand _70px"></div>
                    <div class="fa-brand _70px"></div>
                    <div class="fa-brand _70px"></div>
                    <div class="fa-brand _70px"></div>
                    <div class="fa-brand _70px"></div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- <div class="section has-bg-accent">
        <div class="container is-wide">
            <div class="col block-centered text-align-center lg-6 md-12">
                <h2 class="size-h1">Shop by occasion</h2>
                <div class="low-text-contrast">Looks for everything on your schedule</div>
            </div>
        </div>
        <div class="container is-wide flexh-align-center">
            <div class="col lg-4 sm-12 lg-md-no-padding-lr sm-margin-bottom-double">
                <div class="text-align-center"><img src="https://via.placeholder.com/1000x600.png?text=IMAGE" alt="" class="margin-bottom-double">
                    <div class="size-h3 margin-bottom-small">Business</div>
                    <div class="xs-margin-bottom margin-bottom">Put-together looks are always the best strategy</div>
                    <a href="#" class="button-primary is-ghost w-inline-block">
                        <div class="button-primary-text">Browse collection</div>
                    </a>
                </div>
            </div>
            <div class="col lg-4 sm-12 lg-md-no-padding-lr sm-margin-bottom-double">
                <div class="text-align-center"><img src="https://via.placeholder.com/1000x600.png?text=IMAGE" alt="" class="margin-bottom-double">
                    <div class="size-h3 margin-bottom-small">Weekend</div>
                    <div class="xs-margin-bottom margin-bottom">More reason to love these two days of the week</div>
                    <a href="#" class="button-primary is-ghost w-inline-block">
                        <div class="button-primary-text">Browse collection</div>
                    </a>
                </div>
            </div>
            <div class="col lg-4 sm-12 lg-md-no-padding-lr sm-margin-bottom-double">
                <div class="text-align-center"><img src="https://via.placeholder.com/1000x600.png?text=IMAGE" alt="" class="margin-bottom-double">
                    <div class="size-h3 margin-bottom-small">Casual</div>
                    <div class="margin-bottom">Keep your head in the game in these jackets</div>
                    <a href="#" class="button-primary is-ghost w-inline-block">
                        <div class="button-primary-text">Browse collection</div>
                    </a>
                </div>
            </div>
        </div>
    </div> -->
    
    <div class="section has-bg-accent">
        <div class="container is-wide">
            <div class="col block-centered text-align-center lg-6 md-12">
                <h2 class="size-h1">Order Now</h2>
                <div class="low-text-contrast" style="color:#000;">Temukan di toko online favorit anda</div>
            </div>
        </div>
        <div class="container is-wide">

            <!-- <div class="col lg-2 md-4 xs-6"><a href="https://www.tokopedia.com/360boxershorts" class="text-align-center w-inline-block"><img src="asset/images/logotoped.png" alt="" class="is-rounded margin-bottom">
                    <div class="size-h4">Tokopedia</div>
                </a></div>
            <div class="col lg-2 md-4 xs-6"><a href="#" class="text-align-center w-inline-block"><img src="asset/images/logosp.jpeg" alt="" class="is-rounded margin-bottom">
                    <div class="size-h4">Shopee</div>
                </a></div>
            <div class="col lg-2 md-4 xs-6"><a href="#" class="text-align-center w-inline-block"><img src="asset/images/logobl.png" alt="" class="is-rounded margin-bottom">
                    <div class="size-h4">Bukalapak</div>
                </a></div>
            <div class="col lg-2 md-4 xs-6"><a href="#" class="text-align-center w-inline-block"><img src="asset/images/logotoped.png" alt="" class="is-rounded margin-bottom">
                    <div class="size-h4">Tokopedia</div>
                </a></div>
            <div class="col lg-2 md-4 xs-6"><a href="#" class="text-align-center w-inline-block"><img src="asset/images/logosp.jpeg" alt="" class="is-rounded margin-bottom">
                    <div class="size-h4">Shopee</div>
                </a></div>
            <div class="col lg-2 md-4 xs-6"><a href="#" class="text-align-center w-inline-block"><img src="asset/images/logobl.png" alt="" class="is-rounded margin-bottom">
                    <div class="size-h4">Bukalapak</div>
                </a></div>
        </div> -->
        <table>
            <tr class="container">
                <td  class="col lg-4 md-12 xs-12">
                </td>
                <td  class="col lg-3 md-12 xs-12" class="col lg-2 md-4 xs-6" ><div class="col lg-2 md-4 xs-6"><a href="https://www.tokopedia.com/360boxershorts" class="text-align-center w-inline-block"><img src="asset/images/logotoped.png" alt="" class="is-rounded margin-bottom">
                    <div class="size-h4">Tokopedia</div>
                    </a></div>
                </td>
                <td  class="col lg-3 md-12 xs-12"><div class="col lg-2 md-4 xs-6"><a href="#" class="text-align-center w-inline-block"><img src="asset/images/logosp.jpeg" alt="" class="is-rounded margin-bottom">
                        <div class="size-h4">Shopee</div>
                    </a></div>
                </td>
                <td  class="col lg-2 md-12 xs-12" class="col lg-2 md-4 xs-6" >
                </td>
            </tr>
        </table>
    </div>
    <div class="section sec-contact">
        <div class="container">
            <div class="col lg-12 md-12">
                <h2>👋   Hello let&#x27;s get in touch</h2>
                <p class="text-medium low-text-contrast" style="color:#000;">Tetap terhubung dengan kami Lengkapi form ini dan ajukan pertanyaan tentang kami <a href="mailto:support@360boxershorts.com">support@360boxershorts.com</a></p>
                <div class="w-form">
                    <form id="email-form" name="email-form" data-name="Email Form" method="post" action="<?php echo base_url(); ?>index.php/Home/sendMessage">
                        <input type="text" class="form-input-text style1 w-input" maxlength="256" name="name" data-name="Name" placeholder="Name" id="name">
                        <input type="email" class="form-input-text style1 w-input" maxlength="256" name="email" data-name="Email 3" placeholder="Email" id="email-3" required="">
                        <input type="tel" class="form-input-text style1 w-input" maxlength="256" name="phone" data-name="Phone" placeholder="Phone" id="Phone" required="">
                        <textarea placeholder="Your Message" maxlength="5000" id="Message" name="message" data-name="Message" required="" class="form-input-text textarea style1 w-input"></textarea>
                        <input type="submit" value="Submit" data-wait="Please wait..." class="button-primary margin-top w-button">
                    </form>
                    <div class="w-form-done">
                        <div>Thank you! Your submission has been received!</div>
                    </div>
                    <div class="w-form-fail">
                        <div>Oops! Something went wrong while submitting the form.</div>
                    </div>
                </div>
            </div>
            <!-- <div class="col lg-2 no-margin-bottom"></div> -->
            <!-- <div class="col lg-4 md-12">
                <div class="container container-nested">
                    <div class="col lg-2 no-padding-right"><img src="https://via.placeholder.com/1000x600.png?text=IMAGE" width="64" alt="" class="margin-top-negative"></div>
                    <div class="col lg-12">
                        <div class="size-h4 margin-bottom">Contact Information</div>
                        <div class="margin-bottom">Jalan Ratna 68G, Gatsu Timur<br>Bali — Indonesia Raya</div>
                        <div class="margin-bottom-double">Call Us: +623 456 789 10<br>We are open from Monday to Friday<br>09.00am — 05.00pm</div>
                        <div class="size-h4 margin-bottom">Follow Us</div>
                        <div class="flexh-space-between low-text-contrast">
                            <div class="fa-brand margin-right-small"><a href="#"></a></div>
                            <div class="fa-brand margin-right-small"><a href="#"></a></div>
                            <div class="fa-brand margin-right-small"><a href="#"></a></div>
                            <div class="fa-brand margin-right-small"><a href="#"></a></div>
                            <div class="fa-brand margin-right-small"><a href="#"></a></div>
                            <div class="fa-brand margin-right-small"><a href="#"></a></div>
                        </div>
                    </div>
                </div>
            </div> -->
        </div>
    </div>
    
    <!-- <div class="section">
        <div class="container is-wide">
            <div class="col block-centered text-align-center lg-6 md-12">
                <h2 class="size-h1">Trending now</h2>
            </div>
        </div>
        <div class="container is-wide">
            <div class="col lg-12">
                <div class="w-layout-grid c-grid1x4">
                    <div class="c-product-thumb">
                        <div class="c-product-thumb__top"><a href="single-product.html" class="margin-bottom-small w-inline-block"><img src="https://via.placeholder.com/1000x600.png?text=IMAGE 560w" sizes="(max-width: 479px) 90vw, (max-width: 615px) 91vw, 560px" alt="">
                                <div class="c-product-thumb__discount">
                                    <div>45%</div>
                                </div>
                            </a>
                            <div class="text-xsmall weight-is-medium margin-bottom-small"><a href="#">Bag</a>, <a href="#">Trousers</a></div>
                            <div class="c-product__rating">
                                <div class="fa is-filled"></div>
                                <div class="fa is-filled"></div>
                                <div class="fa is-filled"></div>
                                <div class="fa is-filled"></div>
                                <div class="fa is-blank"></div>
                            </div>
                            <a href="single-product.html" class="w-inline-block">
                                <div class="c-product__title">New Look oversized long sleeve cuff t-shirt in black</div>
                            </a>
                        </div>
                        <div class="c-product__price-wrapper">
                            <div class="c-product__price margin-right-small">$20</div>
                            <div class="c-product__price is-slashed">$45</div>
                        </div>
                        <div class="c-product__quickview">
                            <a href="#" class="button-primary is-small w-inline-block">
                                <div class="button-primary-text"><span class="fa-regular is-16px"></span>  Quick view</div>
                            </a>
                        </div>
                    </div>
                    <div class="c-product-thumb">
                        <div class="c-product-thumb__top"><a href="single-product.html" class="margin-bottom-small w-inline-block"><img src="https://via.placeholder.com/1000x600.png?text=IMAGE 560w" sizes="(max-width: 479px) 90vw, (max-width: 615px) 91vw, 560px" alt="">
                                <div class="c-product-thumb__discount">
                                    <div>5%</div>
                                </div>
                            </a>
                            <div class="text-xsmall weight-is-medium margin-bottom-small"><a href="#">Shorts</a>, <a href="#">Workout</a></div>
                            <div class="c-product__rating">
                                <div class="fa is-filled"></div>
                                <div class="fa is-filled"></div>
                                <div class="fa is-filled"></div>
                                <div class="fa is-filled"></div>
                                <div class="fa is-blank"></div>
                            </div>
                            <a href="single-product.html" class="w-inline-block">
                                <div class="c-product__title">Selected Homme slim fit gingham shirt in blue</div>
                            </a>
                        </div>
                        <div class="c-product__price-wrapper">
                            <div class="c-product__price margin-right-small">$32</div>
                            <div class="c-product__price is-slashed">$42</div>
                        </div>
                        <div class="c-product__quickview">
                            <a href="#" class="button-primary is-small w-inline-block">
                                <div class="button-primary-text"><span class="fa-regular is-16px"></span>  Quick view</div>
                            </a>
                        </div>
                    </div>
                    <div class="c-product-thumb">
                        <div class="c-product-thumb__top"><a href="single-product.html" class="margin-bottom-small w-inline-block"><img src="https://via.placeholder.com/1000x600.png?text=IMAGE 560w" sizes="(max-width: 479px) 90vw, (max-width: 615px) 91vw, 560px" alt="">
                                <div class="c-product-thumb__discount">
                                    <div>8%</div>
                                </div>
                            </a>
                            <div class="text-xsmall weight-is-medium margin-bottom-small"><a href="#">Accessories</a>, <a href="#">Sunglasses</a></div>
                            <div class="c-product__rating">
                                <div class="fa is-filled"></div>
                                <div class="fa is-filled"></div>
                                <div class="fa is-filled"></div>
                                <div class="fa is-filled"></div>
                                <div class="fa is-blank"></div>
                            </div>
                            <a href="single-product.html" class="w-inline-block">
                                <div class="c-product__title">Hollister icon logo slim fit buttondown oxford shirt in black</div>
                            </a>
                        </div>
                        <div class="c-product__price-wrapper">
                            <div class="c-product__price margin-right-small">$19</div>
                        </div>
                        <div class="c-product__quickview">
                            <a href="#" class="button-primary is-small w-inline-block">
                                <div class="button-primary-text"><span class="fa-regular is-16px"></span>  Quick view</div>
                            </a>
                        </div>
                    </div>
                    <div class="c-product-thumb">
                        <div class="c-product-thumb__top"><a href="single-product.html" class="margin-bottom-small w-inline-block"><img src="https://via.placeholder.com/1000x600.png?text=IMAGE 560w" sizes="(max-width: 479px) 90vw, (max-width: 615px) 91vw, 560px" alt="">
                                <div class="c-product-thumb__discount">
                                    <div>98%</div>
                                </div>
                            </a>
                            <div class="text-xsmall weight-is-medium margin-bottom-small"><a href="#">Trousers</a>, <a href="#">Shoes</a></div>
                            <div class="c-product__rating">
                                <div class="fa is-filled"></div>
                                <div class="fa is-filled"></div>
                                <div class="fa is-filled"></div>
                                <div class="fa is-filled"></div>
                                <div class="fa is-blank"></div>
                            </div>
                            <a href="#" class="w-inline-block">
                                <div class="c-product__title">ZUMA DESIGN organic cotton stripe t-shirt in red and white</div>
                            </a>
                        </div>
                        <div class="c-product__price-wrapper">
                            <div class="c-product__price margin-right-small">$22</div>
                        </div>
                        <div class="c-product__quickview">
                            <a href="#" class="button-primary is-small w-inline-block">
                                <div class="button-primary-text"><span class="fa-regular is-16px"></span>  Quick view</div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- <div class="section position-relative is-dark bg-cta">
        <div class="container position-relative">
            <div class="col lg-8 block-centered md-12">
                <div class="text-align-center size-h2 on-dark margin-bottom-double">Subscribe to our Newsletter and get 40% off on all products</div>
                <div class="max-width-500px block-centered no-margin-bottom w-form">
                    <form id="email-form" name="email-form" data-name="Email Form" class="flexh-align-center xs-is-wrapping"><input type="email" class="form-input-text no-margin-bottom lg-md-sm-margin-right-small xs-margin-bottom w-input" maxlength="256" name="email-2" data-name="Email 2" placeholder="Enter your email address" id="email-2" required=""><input type="submit" value="Subscribe" data-wait="Please wait..." class="button-primary is-dark w-button"></form>
                    <div class="w-form-done">
                        <div>Thank you! Your submission has been received!</div>
                    </div>
                    <div class="w-form-fail">
                        <div>Oops! Something went wrong while submitting the form.</div>
                    </div>
                </div>
            </div>
        </div><img src="http://detheme.com/templates/zuma/images/brush.svg" alt="" class="cta-bg">
    </div> -->
    <!-- <div class="section is-dark">
        <div class="container is-wide">
            <div class="col lg-4 md-12 flexh-align-top no-margin-bottom-lg">
                <div class="fa is-24px is-brand-color margin-right"></div>
                <div>
                    <div class="size-h4 margin-bottom-small on-dark">Easy return</div>
                    <div class="low-text-contrast text-small">30 day return window without any question asked, and  also get a full refund, simply a peace of mind.</div>
                </div>
            </div>
            <div class="col lg-4 md-12 flexh-align-top no-margin-bottom-lg">
                <div class="fa is-24px is-brand-color margin-right"></div>
                <div>
                    <div class="size-h4 margin-bottom-small on-dark">Global shipping</div>
                    <div class="low-text-contrast text-small">We accept order around the globe and deliver to more than 120 countries in just 24 hours to your door.</div>
                </div>
            </div>
            <div class="col lg-4 md-12 flexh-align-top no-margin-bottom">
                <div class="fa is-24px is-brand-color margin-right"></div>
                <div>
                    <div class="size-h4 margin-bottom-small on-dark">Secure payment</div>
                    <div class="low-text-contrast text-small">All transactions are encrypted with the encryption of standard SSL with 128-bit symmetric key</div>
                </div>
            </div>
        </div>
    </div> -->
    
    
<!-- Modal -->
<div id="myModal1" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Online Store</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body container">
        <div class="col lg-2 md-4 xs-6"><a href="https://www.tokopedia.com/360boxershorts/celana-boxer-pria-shark-kiddos-ukuran-l" class="text-align-center w-inline-block"><img src="asset/images/logotoped.png" alt="" class="is-rounded margin-bottom"><div class="size-h4">Tokopedia</div></a></div>
        <div class="col lg-2 md-4 xs-6"><a href="https://shopee.co.id/CELANA-BOXER-PRIA-SHARK-KIDDOS-i.306088420.6951270037" class="text-align-center w-inline-block"><img src="asset/images/logosp.jpeg" alt="" class="is-rounded margin-bottom"><div class="size-h4">Shopee</div></a></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<!-- Modal -->
<div id="myModal2" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Online Store</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body container">
        <div class="col lg-2 md-4 xs-6"><a href="https://www.tokopedia.com/360boxershorts/celana-boxer-pria-fast-food-ukuran-m" class="text-align-center w-inline-block"><img src="asset/images/logotoped.png" alt="" class="is-rounded margin-bottom"><div class="size-h4">Tokopedia</div></a></div>
        <div class="col lg-2 md-4 xs-6"><a href="https://shopee.co.id/CELANA-BOXER-PRIA-FAST-FOOD-i.306088420.6651266270" class="text-align-center w-inline-block"><img src="asset/images/logosp.jpeg" alt="" class="is-rounded margin-bottom"><div class="size-h4">Shopee</div></a></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<!-- Modal -->
<div id="myModal3" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Online Store</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body container">
        <div class="col lg-2 md-4 xs-6"><a href="https://www.tokopedia.com/360boxershorts/celana-boxer-pria-manly-greys-m" class="text-align-center w-inline-block"><img src="asset/images/logotoped.png" alt="" class="is-rounded margin-bottom"><div class="size-h4">Tokopedia</div></a></div>
        <div class="col lg-2 md-4 xs-6"><a href="https://shopee.co.id/CELANA-BOXER-PRIA-MANLY-GREYS-i.306088420.6951270092" class="text-align-center w-inline-block"><img src="asset/images/logosp.jpeg" alt="" class="is-rounded margin-bottom"><div class="size-h4">Shopee</div></a></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<!-- Modal -->
<div id="myModal4" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Online Store</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body container">
        <div class="col lg-2 md-4 xs-6"><a href="https://www.tokopedia.com/360boxershorts/celana-boxer-pria-bluestera-ukuran-m" class="text-align-center w-inline-block"><img src="asset/images/logotoped.png" alt="" class="is-rounded margin-bottom"><div class="size-h4">Tokopedia</div></a></div>
        <div class="col lg-2 md-4 xs-6"><a href="https://shopee.co.id/CELANA-BOXER-PRIA-BLUESTERA-i.306088420.6751266235" class="text-align-center w-inline-block"><img src="asset/images/logosp.jpeg" alt="" class="is-rounded margin-bottom"><div class="size-h4">Shopee</div></a></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<!-- Modal -->
<div id="myModal5" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Online Store</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body container">
        <div class="col lg-2 md-4 xs-6"><a href="https://www.tokopedia.com/360boxershorts/celana-boxer-pria-olive-phapilos-m" class="text-align-center w-inline-block"><img src="asset/images/logotoped.png" alt="" class="is-rounded margin-bottom"><div class="size-h4">Tokopedia</div></a></div>
        <div class="col lg-2 md-4 xs-6"><a href="https://shopee.co.id/CELANA-BOXER-PRIA-OLIVE-PHAPILOS-i.306088420.7051270000" class="text-align-center w-inline-block"><img src="asset/images/logosp.jpeg" alt="" class="is-rounded margin-bottom"><div class="size-h4">Shopee</div></a></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>