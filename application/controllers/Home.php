<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 
	private $web_name = "360 Boxer Shorts";
	 
	public function index()
	{
		$this->load->view('templates/header-tanpa-menu');
		$this->load->view('index');
		$this->load->view('templates/footer');
	}
	
	function web_name()
    {
        return $this->web_name;
    }
	
	public function sendMessage()
	{
		// $this->load->library('session');

		$name = $this->input->post('name');
		$email = $this->input->post('email');
		$phone = $this->input->post('phone');
		$message = $this->input->post('message');

		$data['name'] = $name;
		$data['email'] = $email;
		$data['phone'] = $phone;
		$data['messaage'] = $message;
		
		$subject=$name."/ ".$email."/ ".$phone;
		$to_email="support@360boxershorts.com";
		$to_name="Admin 360 Boxer Shorts";
		$body=$message;
		

		$this->load->library('my_phpmailer');
        $mail = new PHPMailer;

        try {
            $mail->IsSMTP();
            $mail->SMTPSecure = "ssl";
            $mail->Host = "mail.360boxershorts.com"; //hostname masing-masing provider email
            $mail->SMTPDebug = 2;
            $mail->SMTPDebug = FALSE;
            $mail->do_debug = 0;
            $mail->Port = 465;
            $mail->SMTPAuth = true;
            $mail->Username = "support@360boxershorts.com"; //user email
            $mail->Password = "2UdTbRc#2B?m"; //password email
            $mail->SetFrom("support@360boxershorts.com", $this->web_name); //set email pengirim
            $mail->Subject = $subject; //subyek email
            $mail->AddAddress($to_email, $to_name); //tujuan email
            $mail->MsgHTML($body);
            $mail->Send();
            //echo "Message has been sent";
        } catch (phpmailerException $e) {
            echo $e->errorMessage(); //Pretty error messages from PHPMailer
            
        } catch (Exception $e) {
            echo $e->getMessage(); //Boring error messages from anything else!
        }
		
		return redirect()->to('index');
		
	}
	
}
