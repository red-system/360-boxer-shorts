<?php

Class Main
{

    private $ci;
    private $web_name = "Ori's Cake";
    private $file_info = 'Images with resolution 800px x 600px and size 100KB';
    private $file_info_slider = 'Images with resolution 1920px x 900px and size 250KB';
    private $path_images = 'upload/images/';
    private $image_size_preview = 200;
    private $help_thumbnail_alt = 'Penting untuk SEO Gambar';
    private $help_meta = 'Penting untuk SEO Halaman Website';
    private $short_desc_char = 100;

    public function __construct()
    {
        error_reporting(0);
        $this->ci =& get_instance();
    }

    function short_desc($string)
    {
        return substr(strip_tags($string), 0, $this->short_desc_char) . ' ...';
    }

    function web_name()
    {
        return $this->web_name;
    }

    function date_view($date)
    {
        return date('d F Y', strtotime($date));
    }

    function help_thumbnail_alt()
    {
        return $this->help_thumbnail_alt;
    }

    function help_meta()
    {
        return $this->help_meta;
    }

    function file_info()
    {
        return $this->file_info;
    }

    function file_info_slider()
    {
        return $this->file_info_slider;
    }

    function path_images()
    {
        return $this->path_images;
    }

    function image_size_preview()
    {
        return $this->image_size_preview;
    }

    function image_preview_url($filename)
    {
        return base_url($this->path_images . $filename);
    }

    function delete_file($filename)
    {
        if ($filename) {
            if (file_exists(FCPATH . $this->path_images . $filename)) {
//				/unlink($this->path_images . $filename);
            }
        }
    }
    function menu_layanan(){
         $id_language = $this->ci->session->userdata('id_language') ?
            $this->ci->session->userdata('id_language') :
            $this->ci->db->select('id')->where('use', 'yes')->order_by('id', 'ASC')->get('language')->row()->id;
         $data = $this->ci->db
             ->select('id,title')
             ->where('id_language', $id_language)
             ->where('category', 'layanan')
             ->get('layanan')
             ->result();
         return $data;
    }
    function data_main($menu_active=null)
    {
        $id_language = $this->ci->session->userdata('id_language') ?
            $this->ci->session->userdata('id_language') :
            $this->ci->db->select('id')->where('use', 'yes')->order_by('id', 'ASC')->get('language')->row()->id;

        $data = array(
            'web_name' => $this->web_name,
            'menu_list' => $this->menu_list(),
            'name' => $this->ci->session->userdata('name'),
            'language' => $this->ci->db->where('use', 'yes')->get('language')->result(),
            'id_language' => $id_language,
        );

        $tab_language = $this->ci->load->view('admins/components/tab_language', $data, TRUE);
        $data['tab_language'] = $tab_language;
        $data['current_url'] = $menu_active ? $menu_active : current_url();

        return $data;
    }

    function data_front()
    {

        $lang_code = 'id'; //$this->ci->lang->lang();
        $lang_active = $this->ci->db->where('code', $lang_code)->get('language')->row();
        $footer = $this->ci->db->select('description')->where('type', 'footer')->get('pages')->row()->description;
//		$service_list = $this->ci->db->select('title,id')->where('id_language', $lang_active->id)->order_by('title', 'ASC')->get('category')->result();
//		$language_list = $this->ci->db->where('use','yes')->order_by('title','ASC')->get('language')->result();

        $data = array(
            'nama' => "Ori's Cake",
            'favicon' => 'oriscake-favicon.png',
            'logo_header' => 'oriscake-logo.png',
            'logo_footer' => '',
            'alamat' => 'Jl. Nangka Utara, Kel. Tonja, Kec. Denpasar Utara, Denpasar, Bali 80239',
            'telephone' => '',
            'telephone_link' => '',
            'jam_kerja' => 'Senin - Sabtu : 08:00 - 20:00',
            'phone' => '081258143623',
            'phone_link' => 'tel:081258143623',
            'whatsapp' => '081258143623',
            'whatsapp_link' => 'https://wa.me/6281258143623?text=Saya%20tertarik%20dengan%20kue%20Anda%20yang%20jual',
            'wechat_id' => '',
            'wechat_link' => '',
            'email_link' => 'mailto:oriscake@gmail.com',
            'email' => 'oriscake@gmail.com',
            'facebook_link' => '',
            'facebook_title' => '',
            'instagram_link' => 'https://www.instagram.com/oris_cake/',
            'instagram_title' => "Ori's Cake",
            'linkedin_link' => '',
            'linkedin_title' => '',
            'view_secret' => FALSE,
            'author' => 'https://oriscake.com/',
//			'services_list' => $service_list,
//			'language_list' => $language_list,
            'lang_code' => $lang_code,
            'lang_active' => $lang_active,
            'id_language' => $lang_active->id,
            'footer' => $footer
        );

        return $data;
    }

    function check_admin()
    {
        if ($this->ci->session->userdata('status') !== 'login') {
            redirect('proweb');
        }
    }

    function check_member()
    {
        if ($this->ci->session->userdata('status') !== 'login') {
            redirect('login');
        }
    }

    function check_login()
    {
        if ($this->ci->session->userdata('status') == 'login') {
            redirect('proweb/dashboard');
        }
    }


    function permalink($data)
    {

        $slug = '';
        foreach ($data as $r) {
            $slug .= $this->slug($r) . '/';
        }

        return site_url($slug);
    }

    function string_to_number($string){
		$temp = str_replace(".", "", $string);
		$temp = str_replace(",", "", $temp);
		return $temp;
	}

    function breadcrumb($data)
    {
        $breadcrumb = '<ul class="breadcrumb">';
        $count = count($data);
        $no = 1;
        foreach ($data as $url => $label) {
            $current = '';
            if ($no == $count) {
                $current = ' class="current"';
            }

            $breadcrumb .= '<li' . $current . '><a href="' . $url . '">' . $label . '</a></li>';
        }

        $breadcrumb .= '</ul>';


        return $breadcrumb;
    }
    function get_hari($data){
        $hari = $data;

        switch($hari){
            case 'Sun':
                $hari_ini = "Minggu";
            break;

            case 'Mon':
                $hari_ini = "Senin";
            break;

            case 'Tue':
                $hari_ini = "Selasa";
            break;

            case 'Wed':
                $hari_ini = "Rabu";
            break;

            case 'Thu':
                $hari_ini = "Kamis";
            break;

            case 'Fri':
                $hari_ini = "Jumat";
            break;

            case 'Sat':
                $hari_ini = "Sabtu";
            break;

            default:
                $hari_ini = "Tidak di ketahui";
            break;
        }

        return $hari_ini;

    }

    function slug($text)
    {

        $find = array(' ', '/', '&', '\\', '\'', ',','(',')');
        $replace = array('-', '-', 'and', '-', '-', '-','','');

        $slug = str_replace($find, $replace, strtolower($text));

        return $slug;
    }

    function date_format_view($date)
    {
        return date('d M Y', strtotime($date));
    }

    function slug_back($slug)
    {
        $slug = trim($slug);
        if (empty($slug)) return '';
        $slug = str_replace('-', ' ', $slug);
        $slug = ucwords($slug);
        return $slug;
    }

    function upload_file_thumbnail($fieldname, $filename)
    {
        $config['upload_path'] = './upload/images/';
        $config['allowed_types'] = '*';
        $config['max_size'] = 10000;
        $config['max_width'] = 80000;
        $config['max_height'] = 60000;
//		$config['overwrite'] = TRUE;
        $config['file_name'] = $this->slug($filename);
        $this->ci->load->library('upload', $config);

        if (!$this->ci->upload->do_upload($fieldname)) {
            return array(
                'status' => FALSE,
                'message' => $this->ci->upload->display_errors()
            );
        } else {
            return array(
                'status' => TRUE,
                'filename' => $data['thumbnail'] = $this->ci->upload->file_name
            );
        }
    }

    function upload_file_slider($fieldname, $filename)
    {
        $config['upload_path'] = './upload/images/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 250;
        $config['max_width'] = 1920;
        $config['max_height'] = 1280;
//		$config['overwrite'] = TRUE;
        $config['file_name'] = $this->slug($filename);
        $this->ci->load->library('upload', $config);

        if (!$this->ci->upload->do_upload($fieldname)) {
            return array(
                'status' => FALSE,
                'message' => $this->ci->upload->display_errors()
            );
        } else {
            return array(
                'status' => TRUE,
                'filename' => $data['thumbnail'] = $this->ci->upload->file_name
            );
        }
    }

    function captcha()
    {
        $this->ci->load->helper(array('captcha', 'string'));
        $this->ci->load->library('session');

        $vals = array(
            'img_path' => './upload/images/captcha/',
            'img_url' => base_url() . 'upload/images/captcha',
            'font_path'  => './assets/css/fonts/mvboli.ttf',
            'img_width' => '200',
            'img_height' => 35,
            'border' => 0,
            'expiration' => 7200,
            'word' => random_string('numeric', 5)
        );

        // create captcha image
        $cap = create_captcha($vals);

        // store image html code in a variable
        $captcha = $cap['image'];

        // store the captcha word in a session
        //$cap['word'];
        $this->ci->session->set_userdata('captcha_mwz', $cap['word']);

        return $captcha;
    }

    function share_link($socmed_type, $title, $link) {
        switch($socmed_type) {
            case "facebook":
                return "https://www.facebook.com/sharer/sharer.php?u=".$link;
                break;
            case "twitter":
                return "https://twitter.com/home?status=".$link;
                break;
            case "googleplus":
                return "https://plus.google.com/share?url=".$link;
                break;
            case "linkedin":
                return "https://www.linkedin.com/shareArticle?mini=true&url=".$link."&title=".$title."&summary=&source=";
                break;
            case "pinterest":
                return "https://pinterest.com/pin/create/button/?url=".$title."&media=".$link."&description=";
                break;
            case "email":
                return "mailto:".$link."?&subject=".$title;
            default:
                return $link;
                break;
        }
    }

    function mailer_auth($subject, $to_email, $to_name, $body)
    {
        $this->ci->load->library('my_phpmailer');
        $mail = new PHPMailer;

        try {
            $mail->IsSMTP();
            $mail->SMTPSecure = "ssl";
            $mail->Host = "mail.oriscake.com"; //hostname masing-masing provider email
            $mail->SMTPDebug = 2;
            $mail->SMTPDebug = FALSE;
            $mail->do_debug = 0;
            $mail->Port = 465;
            $mail->SMTPAuth = true;
            $mail->Username = "support@oriscake.com"; //user email
            $mail->Password = "uJwvL~ET(t2y"; //password email
            $mail->SetFrom("support@oriscake.com", $this->web_name); //set email pengirim
            $mail->Subject = $subject; //subyek email
            $mail->AddAddress($to_email, $to_name); //tujuan email
            $mail->MsgHTML($body);
            $mail->Send();
            //echo "Message has been sent";
        } catch (phpmailerException $e) {
            echo $e->errorMessage(); //Pretty error messages from PHPMailer
        } catch (Exception $e) {
            echo $e->getMessage(); //Boring error messages from anything else!
        }
    }

    function menu_list()
    {
        $menu = array(
            'MAIN' => array(
                'dashboard' => array(
                    'label' => 'Dashboard',
                    'route' => base_url('proweb/dashboard'),
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array()
                ),
                'view_front' => array(
                    'label' => 'View Website',
                    'route' => base_url(''),
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array()
                )
            ),
            'PAGES' => array(
                'home' => array(
                    'label' => 'Beranda',
                    'route' => 'javascript:;',
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array(
                        'home_page' => array(
                            'label' => 'Halama Beranda',
                            'route' => base_url('proweb/pages/type/home'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
//                        'sesi_1' => array(
//                            'label' => 'Sesi 1',
//                            'route' => base_url('proweb/pages/type/home_sesi_1'),
//                            'icon' => 'fab fa-asymmetrik'
//                        ),
//                        'sesi_2' => array(
//                            'label' => 'Sesi 2',
//                            'route' => base_url('proweb/pages/type/home_sesi_2'),
//                            'icon' => 'fab fa-asymmetrik'
//                        ),
//                        'sesi_3' => array(
//                            'label' => 'Sesi 3',
//                            'route' => base_url('proweb/pages/type/home_sesi_3'),
//                            'icon' => 'fab fa-asymmetrik'
//                        ),
						'home_slider' => array(
							'label' => 'Home Image',
							'route' => base_url('proweb/home_slider'),
							'icon' => 'fab fa-asymmetrik'
						),
                    )
                ),
                'pertanyaan' => array(
                    'label' => 'Data Pertanyaan',
                    'route' => 'javascript:;',
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array(
                        'pertanyaan_1' => array(
							'label' => 'Pertanyaan 1',
							'route' => base_url('proweb/pertanyaan/layer_1'),
							'icon' => 'fab fa-asymmetrik'
						),
                        'pertanyaan_2' => array(
							'label' => 'Pertanyaan 2',
							'route' => base_url('proweb/pertanyaan/layer_2'),
							'icon' => 'fab fa-asymmetrik'
						),
                        'pertanyaan_3' => array(
							'label' => 'Pertanyaan 3',
							'route' => base_url('proweb/pertanyaan/layer_3'),
							'icon' => 'fab fa-asymmetrik'
						),
                    )
                ),
//                'services' => array(
//                    'label' => 'Layanan Kami',
//                    'route' => 'javascript:;',
//                    'icon' => 'fab fa-asymmetrik',
//                    'sub_menu' => array(
////						'services_page' => array(
////							'label' => 'Services Page',
////							'route' => base_url('proweb/pages/type/services'),
////							'icon' => 'fab fa-asymmetrik'
////						),
//                        'categories' => array(
//                            'label' => 'Halaman Layanan Kami',
//                            'route' => base_url('proweb/pages/type/services'),
//                            'icon' => 'fab fa-asymmetrik'
//                        ),
//                        'services' => array(
//                            'label' => 'Daftar Item Layanan',
//                            'route' => base_url('proweb/layanan'),
//                            'icon' => 'fab fa-asymmetrik'
//                        ),
////                        'services_gallery' => array(
////                            'label' => 'Services Gallery List',
////                            'route' => base_url('proweb/tour_gallery'),
////                            'icon' => 'fab fa-asymmetrik'
////                        ),
//                    )
//                ),
                'pelanggan' => array(
                    'label' => 'Data Pelanggan',
                    'route' => base_url('proweb/pelanggan'),
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array()
                ),
                'transaksi' => array(
                    'label' => 'Transaksi',
                    'route' => 'javascript:;',
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array('kategori_kue' => array(
							'label' => 'Data Order',
							'route' => base_url('proweb/order'),
							'icon' => 'fab fa-asymmetrik'
						),
                    )
                ),

                'management_data_kue' => array(
					'label' => 'Management Data Kue',
					'route' => 'javascript:;',
					'icon' => 'fab fa-asymmetrik',
					'sub_menu' => array(
						'kategori_kue' => array(
							'label' => 'Kategori Kue',
							'route' => base_url('proweb/cake_category'),
							'icon' => 'fab fa-asymmetrik'
						),
						'rasa' => array(
							'label' => 'List Rasa',
							'route' => base_url('proweb/rasa'),
							'icon' => 'fab fa-asymmetrik'
						),
                        'price_list_additional' => array(
							'label' => 'Price List Additional',
							'route' => base_url('proweb/price_list_additional'),
							'icon' => 'fab fa-asymmetrik'
						),
                        'price_list_cake' => array(
							'label' => 'Price List Cake',
							'route' => base_url('proweb/price_list_cake'),
							'icon' => 'fab fa-asymmetrik'
						),
                        'price_list_cupcake' => array(
							'label' => 'Price List Cupcake',
							'route' => base_url('proweb/price_list_cupcake'),
							'icon' => 'fab fa-asymmetrik'
						),
                        'price_list_glass_cake' => array(
							'label' => 'Price List Glass Cake',
							'route' => base_url('proweb/price_list_glass_cake'),
							'icon' => 'fab fa-asymmetrik'
						),
                        'price_list_love' => array(
							'label' => 'Price List Love',
							'route' => base_url('proweb/price_list_love'),
							'icon' => 'fab fa-asymmetrik'
						),
                        'price_list_mini_cake' => array(
							'label' => 'Price List Mini Cake',
							'route' => base_url('proweb/price_list_mini_cake'),
							'icon' => 'fab fa-asymmetrik'
						),
                        'price_list_shape_name' => array(
							'label' => 'Price List Shape Name',
							'route' => base_url('proweb/price_list_shape_name'),
							'icon' => 'fab fa-asymmetrik'
						),
                        'price_list_alphabets_number' => array(
							'label' => 'Price List Alphabets & Numbers',
							'route' => base_url('proweb/price_list_alphabets_numbers'),
							'icon' => 'fab fa-asymmetrik'
						)
					)
				),
                'kue' => array(
					'label' => 'Data Kue',
					'route' => 'javascript:;',
					'icon' => 'fab fa-asymmetrik',
					'sub_menu' => array(
						'kue_page' => array(
							'label' => 'Halama Daftar Kue',
							'route' => base_url('proweb/pages/type/daftar_kue'),
							'icon' => 'fab fa-asymmetrik'
						),
                        'Cake' => array(
							'label' => 'Daftar Kue',
							'route' => base_url('proweb/cake'),
							'icon' => 'fab fa-asymmetrik'
						),

					)
				),
                'cart' => array(
                    'label' => 'Halaman Keranjang',
                    'route' => base_url('proweb/pages/type/cart'),
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array()
                ),
                'checkout' => array(
                    'label' => 'Halaman Checkout',
                    'route' => base_url('proweb/pages/type/checkout'),
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array()
                ),
                'testimonial' => array(
					'label' => 'Testimonial',
					'route' => 'javascript:;',
					'icon' => 'fab fa-asymmetrik',
					'sub_menu' => array(
						'testimonial_page' => array(
							'label' => 'Halama Testimonial',
							'route' => base_url('proweb/pages/type/testimonial'),
							'icon' => 'fab fa-asymmetrik'
						),
						'testimonial_list' => array(
							'label' => 'Testimonial List',
							'route' => base_url('proweb/testimonial'),
							'icon' => 'fab fa-asymmetrik'
						),
					)
				),
                'gallery_photo' => array(
                    'label' => 'Galeri Foto',
                    'route' => 'javascript:;',
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array(
                        'gallery_photo_page' => array(
                            'label' => 'Halaman Galeri Foto',
                            'route' => base_url('proweb/pages/type/gallery_photo'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'gallery_photo_list' => array(
                            'label' => 'Daftar Galeri Foto',
                            'route' => base_url('proweb/gallery_photo'),
                            'icon' => 'fab fa-asymmetrik'

                        ),
                    )
                ),
                'gallery_video' => array(
					'label' => 'Gallery Video',
					'route' => 'javascript:;',
					'icon' => 'fab fa-asymmetrik',
					'sub_menu' => array(
						'gallery_photo_page' => array(
							'label' => 'Halama Gallery Video',
							'route' => base_url('proweb/pages/type/gallery_video'),
							'icon' => 'fab fa-asymmetrik'
						),
						'gallery_photo_list' => array(
							'label' => 'Gallery Video List',
							'route' => base_url('proweb/gallery_video'),
							'icon' => 'fab fa-asymmetrik'

						),
					)
				),

                'blog' => array(
                    'label' => 'Blog',
                    'route' => 'javascript:;',
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array(
                        'blog_page' => array(
                            'label' => 'Halama Blog',
                            'route' => base_url('proweb/pages/type/blog'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'blog_category' => array(
                            'label' => 'Kategori Blog',
                            'route' => base_url('proweb/blog_category'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'blog_list' => array(
                            'label' => 'Daftar Blog',
                            'route' => base_url('proweb/blog_content'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                    )
                ),
//                'profile' => array(
//                    'label' => 'Tentang Kami',
//                    'route' => 'javascript:;',
//                    'icon' => 'fab fa-asymmetrik',
//                    'sub_menu' => array(
//                        'profile_page' => array(
//                            'label' => 'Halaman Tentang Kami',
//                            'route' => base_url('proweb/pages/type/profile'),
//                            'icon' => 'fab fa-asymmetrik'
//                        ),
//                        'profile_image' => array(
//                            'label' => 'Page Image',
//                            'route' => base_url('proweb/image_page'),
//                            'icon' => 'fab fa-asymmetrik'
//                        ),
//                        'profil_sesi_0' => array(
//                            'label' => 'Halaman Profile',
//                            'route' => base_url('proweb/pages/type/profil_sesi_0'),
//                            'icon' => 'fab fa-asymmetrik'
//                        ),
//                        'profil_sesi_1' => array(
//                            'label' => 'Halaman Offer',
//                            'route' => base_url('proweb/pages/type/profil_sesi_1'),
//                            'icon' => 'fab fa-asymmetrik'
//                        ),
//                        'offer' => array(
//                            'label' => 'Daftar Item Offer',
//                            'route' => base_url('proweb/offer'),
//                            'icon' => 'fab fa-asymmetrik'
//                        ),
//                        'profil_page_team' => array(
//                            'label' => 'Halaman Team',
//                            'route' => base_url('proweb/pages/type/profil_page_team'),
//                            'icon' => 'fab fa-asymmetrik'
//                        ),
//                        'profile_team' => array(
//                            'label' => 'Daftar Team',
//                            'route' => base_url('proweb/profile_team'),
//                            'icon' => 'fab fa-asymmetrik'
//
//                        ),
//                        'profil_sesi_2' => array(
//                            'label' => 'Halaman Count',
//                            'route' => base_url('proweb/pages/type/profil_sesi_2'),
//                            'icon' => 'fab fa-asymmetrik'
//                        ),
//                        'count' => array(
//                            'label' => 'Daftar Count',
//                            'route' => base_url('proweb/count'),
//                            'icon' => 'fab fa-asymmetrik'
//
//                        ),
//                    )
//                ),


//				'about_us' => array(
//					'label' => 'About Us',
//					'route' => base_url('proweb/pages/type/about_us'),
//					'icon' => 'fab fa-asymmetrik',
//					'sub_menu' => array()
//				),

                'contact_us' => array(
					'label' => 'Kontak Kami',
					'route' => 'javascript:;',
					'icon' => 'fab fa-asymmetrik',
					'sub_menu' => array(
						'contact_us_page' => array(
							'label' => 'Halama Kontak Kami',
							'route' => base_url('proweb/pages/type/contact_us'),
							'icon' => 'fab fa-asymmetrik'
						),
						'contact_us_list' => array(
							'label' => 'Email List',
							'route' => base_url('proweb/kontak_email'),
							'icon' => 'fab fa-asymmetrik'
						),
					)
				),
				'faq' => array(
					'label' => 'FAQ',
					'route' => 'javascript:;',
					'icon' => 'fab fa-asymmetrik',
					'sub_menu' => array(
						'testimonial_page' => array(
							'label' => 'Halama FAQ',
							'route' => base_url('proweb/pages/type/faq'),
							'icon' => 'fab fa-asymmetrik'
						),
						'testimonial_list' => array(
							'label' => 'FAQ List',
							'route' => base_url('proweb/faq'),
							'icon' => 'fab fa-asymmetrik'
						),
					)
                ),

				'syarat_ketentuan' => array(
                    'label' => 'Halama Syarat Ketentuan',
                    'route' => base_url('proweb/pages/type/syarat_ketentuan'),
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array()
                ),
                'kebijakan_privasi' => array(
                    'label' => 'Halama Kebijakan Privasi',
                    'route' => base_url('proweb/pages/type/kebijakan_privasi'),
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array()
                ),
//                'reservation' => array(
//                    'label' => 'Reservation',
//                    'route' => 'javascript:;',
//                    'icon' => 'fab fa-asymmetrik',
//                    'sub_menu' => array(
//                        'reservation_page' => array(
//                            'label' => 'Reservation Page',
//                            'route' => base_url('proweb/pages/type/reservation'),
//                            'icon' => 'fab fa-asymmetrik'
//                        ),
//                        'reservation_list' => array(
//                            'label' => 'Reservation List',
//                            'route' => base_url('proweb/reservation'),
//                            'icon' => 'fab fa-asymmetrik'
//                        ),
//                    )
//                ),
                'data_akun' => array(
                    'label' => 'Data Akun dan Pesanan',
                    'route' => 'javascript:;',
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array(
                        'akun_page' => array(
                            'label' => 'Halaman Data Akun',
                            'route' => base_url('proweb/pages/type/akun'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'order_active' => array(
                            'label' => 'Halaman Pesanan Aktif',
                            'route' => base_url('proweb/pages/type/order_active'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'order_history' => array(
                            'label' => 'Halaman Histori Pesanan',
                            'route' => base_url('proweb/pages/type/order_history'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                    )
                ),
                'footer' => array(
                    'label' => 'Footer Web',
                    'route' => base_url('proweb/pages/type/footer'),
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array()
                ),
            ),
            'OTHERS MENU' => array(
                'email' => array(
                    'label' => 'Email',
                    'route' => base_url('proweb/email'),
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array()
                ),
//                'language' => array(
//                    'label' => 'Language',
//                    'route' => base_url('proweb/language'),
//                    'icon' => 'fab fa-asymmetrik',
//                    'sub_menu' => array()
//                ),
                'admin' => array(
                    'label' => 'Manage Admin',
                    'route' => base_url('proweb/admin'),
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array()
                ),
            ),
        );

        return $menu;
    }
}
